﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Collections.Specialized;
using System.Linq;

namespace Calendist.Web.App
{
    public class GoogleApiAuthorizationUriBuilder
    {
        private readonly IConfiguration _configuration;

        private static readonly Uri GoogleApiAuthorizationBaseUri = new Uri("https://accounts.google.com/o/oauth2/v2/auth");
        private static readonly Uri GoogleApiTokenRequestBaseUri = new Uri("https://oauth2.googleapis.com/token");
        private const string GoogleApiCalendarScope = "https://www.googleapis.com/auth/calendar";

        public GoogleApiAuthorizationUriBuilder(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        private string GetFromConfiguration(string key)
        {
            return _configuration[$"{nameof(GoogleApiAuthorizationUriBuilder)}:{key}"];
        }

        public Uri BuildUriForTokenRequest(string authorizationCode)
        {
            var parameters = new NameValueCollection
            {
                ["code"] = $"{WebUtility.UrlEncode(authorizationCode)}",
                ["client_id"] = GetFromConfiguration("ClientId"),
                ["client_secret"] = GetFromConfiguration("ClientSecret"),
                ["scope"] = null,
                ["grant_type"] = "authorization_code",
                ["redirect_uri"] = $"{WebUtility.UrlEncode(GetFromConfiguration("RedirectUri"))}"
            };
            var uriBuilder = new UriBuilder(GoogleApiTokenRequestBaseUri)
            {
                Query = ToQueryString(parameters)
            };
            return uriBuilder.Uri;
        }

        private string ToQueryString(NameValueCollection nvc)
        {
            return string.Join("&", nvc.AllKeys.Select(k => $"{k}={nvc[k]}"));
        }

        public Uri BuildUriForAuthorization()
        {
            var parameters = new NameValueCollection
            {
                ["access_type"] = "offline",
                ["response_type"] = "code",
                ["client_id"] = GetFromConfiguration("ClientId"),
                ["scope"] = null,
                ["redirect_uri"] = $"{WebUtility.UrlEncode(GetFromConfiguration("RedirectUri"))}",
                ["scope"] = $"{WebUtility.UrlEncode(GoogleApiCalendarScope)}"
            };
            var uriBuilder = new UriBuilder(GoogleApiAuthorizationBaseUri)
            {
                Query = ToQueryString(parameters)
            };
            return uriBuilder.Uri;
        }
    }
}
