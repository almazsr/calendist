﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Calendist.Web.App")]

namespace Calendist
{
    public class CalendarPeriod : IEquatable<CalendarPeriod>
    {
        public class Union
        {
            private List<CalendarPeriod> _periods;

            public Union(params CalendarPeriod[] periods)
            {
                _periods = periods?.ToList() ?? throw new ArgumentNullException(nameof(periods));
            }

            public bool Contains(CalendarPeriod period)
            {
                return DateFrom() <= period.DateFrom() && period.DateTo() <= DateTo();
            }

            public void Switch(CalendarPeriod period, bool include)
            {
                if (include)
                {
                    AddOrNothing(period);
                }
                else
                {
                    RemoveOrNothing(period);
                }
            }

            public void AddOrNothing(CalendarPeriod period)
            {
                if (!_periods.Contains(period))
                {
                    _periods.Add(period);
                }
            }

            public void RemoveOrNothing(CalendarPeriod period)
            {
                if (_periods.Contains(period))
                {
                    _periods.Remove(period);
                }
            }

            public void Add(CalendarPeriod period)
            {
                _periods.Add(period);
            }

            public void Remove(CalendarPeriod period)
            {
                _periods.Remove(period);
            }

            public DateTimeOffset DateFrom() => _periods.Any() ? _periods.Min(p => p.DateFrom()) : DateTimeOffset.MinValue;
            public DateTimeOffset DateTo() => _periods.Any() ? _periods.Max(p => p.DateTo()) : DateTimeOffset.MaxValue;
            public TimeSpan Duration() => DateTo() - DateFrom();

            internal class Data
            {
                public CalendarPeriod.Data[] Periods { get; set; }
            }
            internal static Union FromData(Data data)
            {
                return new Union(data.Periods.Select(CalendarPeriod.FromData).ToArray());
            }
            internal Data ToData()
            {
                return new Data { Periods = _periods.Select(p => p.ToData()).ToArray() };
            }
        }

        public static class Dates
        {
            public static DateTimeOffset Today() => DateTimeOffset.Now.Date;
            public static DateTimeOffset LastWeekMonday() => Today().AddDays(-DayOfWeek(Today()) - 7);
            public static DateTimeOffset Yesterday() => Today().AddDays(-1);
            public static DateTimeOffset Tomorrow() => Today().AddDays(+1);
            public static DateTimeOffset ThisWeekMonday() => Today().AddDays(-DayOfWeek(Today()));

            public static DateTimeOffset NextWeekMonday() => Today().AddDays(-DayOfWeek(Today()) + 7);

            private static int DayOfWeek(DateTimeOffset date)
            {
                var dayOfWeek = (int)date.DayOfWeek - 1;
                if (dayOfWeek < 0)
                {
                    dayOfWeek = 6;
                }
                return dayOfWeek;
            }
        }

        public static class Intervals
        {
            public static TimeSpan Day = TimeSpan.FromDays(1);
            public static TimeSpan Week = TimeSpan.FromDays(7);
        }

        public static CalendarPeriod None = new CalendarPeriod("None", () => DateTimeOffset.MinValue, TimeSpan.FromSeconds(1));
        public static CalendarPeriod LastWeek = new CalendarPeriod("LastWeek", Dates.LastWeekMonday, Intervals.Week);
        public static CalendarPeriod Yesterday = new CalendarPeriod("Yesterday", Dates.Yesterday, Intervals.Day);
        public static CalendarPeriod Today = new CalendarPeriod("Today", Dates.Today, Intervals.Day);
        public static CalendarPeriod Tomorrow = new CalendarPeriod("Tomorrow", Dates.Tomorrow, Intervals.Day);
        public static CalendarPeriod ThisWeek = new CalendarPeriod("ThisWeek", Dates.ThisWeekMonday, Intervals.Week);
        public static CalendarPeriod NextWeek = new CalendarPeriod("NextWeek", Dates.NextWeekMonday, Intervals.Week);

        private static readonly CalendarPeriod[] AllowedPeriods = new CalendarPeriod[] { LastWeek, Yesterday, Today, Tomorrow, ThisWeek, NextWeek };

        private CalendarPeriod(string name, Func<DateTimeOffset> dateFrom, TimeSpan duration)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            if (duration <= TimeSpan.Zero)
            {
                throw new ArgumentException("duration is negative");
            }
            Name = name;
            DateFrom = dateFrom;
            Duration = duration;
        }

        public static implicit operator Union(CalendarPeriod period)
        {
            return new Union(period);
        }

        public string Name { get; private set; }

        public Func<DateTimeOffset> DateFrom { get; private set; }
        public DateTimeOffset DateTo() => DateFrom() + Duration;
        public TimeSpan Duration { get; private set; }

        public static CalendarPeriod FromName(string periodName)
        {
            return AllowedPeriods.First(p => p.Name == periodName);
        }

        public static CalendarPeriod FindMostExactPeriodByDateOrDefault(DateTimeOffset date)
        {
            var candidatePeriods = AllowedPeriods.Where(p => p.Contains(date));
            return candidatePeriods.OrderBy(p => p.Duration).FirstOrDefault() ?? CalendarPeriod.None;
        }

        public bool Contains(DateTimeOffset date)
        {
            return DateFrom() <= date && date < DateTo();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CalendarPeriod);
        }

        public bool Equals(CalendarPeriod other)
        {
            return other != null &&
                   Name == other.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name);
        }

        public static bool operator ==(CalendarPeriod left, CalendarPeriod right)
        {
            return EqualityComparer<CalendarPeriod>.Default.Equals(left, right);
        }

        public static bool operator !=(CalendarPeriod left, CalendarPeriod right)
        {
            return !(left == right);
        }

        internal class Data
        {
            public string Name { get; set; }
        }
        internal static CalendarPeriod FromData(Data data)
        {
            return FromName(data.Name);
        }
        internal Data ToData()
        {
            return new Data { Name = Name };
        }
    }
}
