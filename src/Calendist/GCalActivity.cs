﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Calendist
{
    public class GCalActivity : IEquatable<GCalActivity>
    {
        private const string BasilColorId = "10";
        private const string BasilColor = "#0b8043";
        private readonly GCalEvent _gCalEvent;

        internal GCalActivity(GCalEvent gCalEvent)
        {
            _gCalEvent = gCalEvent;
            var evnt = _gCalEvent.Event;
            Name = evnt.Summary;   
            if (!evnt.Start.DateTime.HasValue)
            {
                throw new InvalidOperationException("Can't initialize with null start date");
            }
            if (!evnt.End.DateTime.HasValue)
            {
                throw new InvalidOperationException("Can't initialize with null end date");
            }
            StartDate = evnt.Start.DateTime.Value;
            var endDate = evnt.End.DateTime.Value;
            Duration = StartDate - endDate;
            IsDone = evnt.ColorId == BasilColorId;
            Intention = _gCalEvent.Calendar.Summary;
            BackgroundColor = IsDone ? BasilColor : _gCalEvent.Calendar.BackgroundColor;
            IsRecurring = evnt.RecurringEventId != null;
        }        

        public bool IsRecurring { get; private set; }
        public string Name { get; private set; }
        public DateTimeOffset StartDate { get; private set; }
        public TimeSpan Duration { get; private set; }

        public string Intention { get; private set; }

        public bool IsDone { get; set; }

        public string BackgroundColor { get; private set; }

        public async Task SetIsDoneAsync(bool isDone)
        {
            var colorId = isDone ? BasilColorId : "0";            
            var evntToPatch = new Google.Apis.Calendar.v3.Data.Event { ColorId = colorId };
            var calendarEventPatchRequest = _gCalEvent.Service.Events.Patch(evntToPatch, _gCalEvent.Calendar.Id, _gCalEvent.Event.Id);
            calendarEventPatchRequest.SendUpdates = Google.Apis.Calendar.v3.EventsResource.PatchRequest.SendUpdatesEnum.All;
            await calendarEventPatchRequest.ExecuteAsync();
            IsDone = isDone;
            BackgroundColor = IsDone ? BasilColor : _gCalEvent.Calendar.BackgroundColor;
        }

        public DateTimeOffset EndDate() => StartDate + Duration;

        public override bool Equals(object obj)
        {
            return Equals(obj as GCalActivity);
        }

        public bool Equals(GCalActivity other)
        {
            return other != null &&
                   Name == other.Name &&
                   StartDate.Equals(other.StartDate) &&
                   Duration.Equals(other.Duration) &&
                   IsDone == other.IsDone &&
                   BackgroundColor == other.BackgroundColor;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, StartDate, Duration, IsDone, BackgroundColor);
        }

        public static bool operator ==(GCalActivity left, GCalActivity right)
        {
            return EqualityComparer<GCalActivity>.Default.Equals(left, right);
        }

        public static bool operator !=(GCalActivity left, GCalActivity right)
        {
            return !(left == right);
        }
    }
}
