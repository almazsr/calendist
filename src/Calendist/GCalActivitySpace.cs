﻿using System;
using System.Collections.Generic;
using Google.Apis.Calendar.v3;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Http;
using Google.Apis.Services;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Reflection;
using System.Runtime.CompilerServices;
using Google.Apis.Calendar.v3.Data;

[assembly: InternalsVisibleTo("Calendist.Web.App")]

namespace Calendist
{
    public class GCalActivitySpace
    {
        internal class AccessTokenHeaderIntereceptor : IHttpExecuteInterceptor, IConfigurableHttpClientInitializer
        {
            private string _accessToken;

            public AccessTokenHeaderIntereceptor(string accessToken)
            {
                _accessToken = accessToken;
            }

            public void Initialize(ConfigurableHttpClient httpClient)
            {
                httpClient.MessageHandler.AddExecuteInterceptor(this);
            }

            public Task InterceptAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _accessToken);
                return Task.FromResult(true);
            }
        }

        private GCalActivitySpace(CalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        public static string[] Scopes = { CalendarService.Scope.Calendar };
        public const string ApplicationName = "Calendist";
        private CalendarService _calendarService;

        // TODO: replace parameters with appropriate object
        public static GCalActivitySpace Create(string accessToken)
        {
            var calendarService = new CalendarService(new BaseClientService.Initializer
            {
                GZipEnabled = false,
                HttpClientInitializer = new AccessTokenHeaderIntereceptor(accessToken)
            });

            return new GCalActivitySpace(calendarService);
        }

        private async Task<IEnumerable<GCalEvent>> GetGCallEventsBetween(DateTime fromDate, DateTime toDate)
        {
            var calendarRequest = _calendarService.CalendarList.List();
            var calendars = await calendarRequest.ExecuteAsync();

            var result = new List<GCalEvent>();
            foreach (var calendar in calendars.Items)
            {
                var calendarId = calendar.Id;
                var calendarEventsRequest = _calendarService.Events.List(calendarId);
                calendarEventsRequest.ShowDeleted = false;
                calendarEventsRequest.TimeMin = fromDate;
                calendarEventsRequest.TimeMax = toDate;
                calendarEventsRequest.SingleEvents = true;
                calendarEventsRequest.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

                var calendarEvents = await calendarEventsRequest.ExecuteAsync();

                foreach (var calendarEvent in calendarEvents.Items)
                {
                    var gCalEvent = new GCalEvent(_calendarService, calendar, calendarEvent);
                    result.Add(gCalEvent);
                }
            }

            return result;
        }

        public async Task<IEnumerable<GCalActivity>> GetActivitiesAsync(DateTimeOffset from, DateTimeOffset to, bool includeDone, bool includeRecurring)
        {
            var today = DateTime.Now.Date;
            var events = (await GetGCallEventsBetween(from.DateTime, to.DateTime)).Where(e => e.Event.Start.DateTime != null && e.Event.End.DateTime != null);

            return events
                .OrderBy(e => e.Event.Start.DateTime)
                .Select(e => new GCalActivity(e))
                .Where(a => includeDone ? true : !a.IsDone)
                .Where(a => includeRecurring ? true : !a.IsRecurring)
                .ToArray();
        }
    }
}
