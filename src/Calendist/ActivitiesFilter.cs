﻿namespace Calendist
{
    public class ActivitiesFilter
    {
        public ActivitiesFilter()
        {
            Period = new CalendarPeriod.Union();
        }

        public bool IncludeDone { get; set; }
        public bool IncludeRecurring { get; set; }
        public CalendarPeriod.Union Period { get; set; }

        public static ActivitiesFilter CreateDefault()
        {
            return new ActivitiesFilter { Period = CalendarPeriod.Today, IncludeRecurring = true };
        }

        internal static ActivitiesFilter FromData(Data data)
        {
            return new ActivitiesFilter
            {
                IncludeDone = data.IncludeDone,
                IncludeRecurring = data.IncludeRecurring,
                Period = CalendarPeriod.Union.FromData(data.Period)
            };
        }
        internal Data ToData()
        {
            return new Data { IncludeDone = IncludeDone, IncludeRecurring = IncludeRecurring, Period = Period.ToData() };
        }
        internal class Data
        {
            public bool IncludeDone { get; set; }
            public bool IncludeRecurring { get; set; }
            public CalendarPeriod.Union.Data Period { get; set; }
        }
    }
}
