﻿using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;

namespace Calendist
{
    internal class GCalEvent
    {
        internal GCalEvent(CalendarService service, CalendarListEntry calendar, Event @event)
        {
            Service = service;
            Calendar = calendar;
            Event = @event;
        }

        internal CalendarService Service { get; }
        public CalendarListEntry Calendar { get; }
        internal Event Event { get; }
    }
}
