#XPlat: .NET Standard, .NET Core, .NET Framework >=4.6 (Latest project format)
#FullFw: .NET Framework <=4.5 (Legacy project format)

function Main
{
	try {
		PrepareArtifactsDirectory
		BuildXPlat
		AnalyzeXPlat
		ConvertCodeQualityReportsXPlat		
		TestXPlat
		GenerateCoverageReportXPlat
	}
	catch {	
		exit 1
	}
}

function GetProjectDirectory
{
	if ($ENV:CI_PROJECT_DIR -eq $null) {
		(Resolve-Path "..").Path
	} else {
		"$ENV:CI_PROJECT_DIR"
	}
}
$projectDirectory=GetProjectDirectory

#Solutions and tests
$xPlatSolution="$projectDirectory\src\Calendist.sln"
$xPlatTestsDirectory="$projectDirectory\test\Calendist.Tests"
	
# Tools
$toolsDirectory="C:\tools"
$dotnetExe="C:\Program Files\dotnet\dotnet.exe"
$reportGeneratorExe="$toolsDirectory\reportgenerator\4.6.4\tools\net47\ReportGenerator.exe"
$metricsExe="$toolsDirectory\Metrics\Metrics\Metrics.exe"
$inspectCodeExe="$toolsDirectory\InspectCode\inspectCode.exe"
$inspectCodeToCodeClimateExe="$toolsDirectory\ConvertInspectCodeToCodeClimate\ConvertInspectCodeToCodeClimate.exe"
$convertMicrosoftMetricsToOpenMetricsExe="$toolsDirectory\ConvertMicrosoftMetricsToOpenMetrics\ConvertMicrosoftMetricsToOpenMetrics.exe"

#Artifact paths
$artifactsDirectory="$projectDirectory\artifacts"
$coverageDirectory="$artifactsDirectory\coverage"
$codeQualityDirectory="$artifactsDirectory\codequality"
$coverageDirectoryXPlat="$coverageDirectory\xplat"
$coverageReportDirectory="$coverageDirectory\report"
$Global:coverageOutputXPlat=$null

function PrepareArtifactsDirectory
{
	"Prepare artifacts directory"
	if (Test-Path $artifactsDirectory) {
		Remove-Item $artifactsDirectory -Recurse
	}
	New-Item -ItemType directory $coverageDirectory | Out-Null
	New-Item -ItemType directory $codeQualityDirectory | Out-Null
}

function BuildXPlat
{
	"Build XPlat solution"	
	dotnet build $xPlatSolution
}

function AnalyzeXPlat
{
	"Inspect XPlat code"
	& $inspectCodeExe $xPlatSolution -a -o="$codeQualityDirectory\XPlat.InspectCode.xml"
	"Calculate XPlat metrics"
	& $metricsExe /solution:$xPlatSolution /out:"$codeQualityDirectory\XPlat.Metrics.xml"
}

function ConvertCodeQualityReportsXPlat
{
	"Output XPlat Code inspections as CodeClimate artifact"
	& $inspectCodeToCodeClimateExe "$codeQualityDirectory\XPlat.InspectCode.xml" "$codeQualityDirectory\codequality.json" $projectDirectory
	"Output XPlat metrics reports as OpenMetrics format"	
	& $convertMicrosoftMetricsToOpenMetricsExe "$codeQualityDirectory\XPlat.Metrics.xml" "$codeQualityDirectory\metrics.txt"
}

function TestXPlat
{
	"Run and calculate cover of XPlat tests"
	dotnet test $xPlatTestsDirectory --collect:"XPlat Code Coverage" -r $coverageDirectoryXPlat
	Get-ChildItem $coverageDirectoryXPlat -Filter "*coverage.cobertura.xml" -Recurse | ForEach {$Global:coverageOutputXPlat=$Global:coverageOutputXPlat + "`"" + $_.Directory + "\" + $_ + "`" ";}
}

function GenerateCoverageReportXPlat
{
	"Generate coverage report"	
	& $reportGeneratorExe "-reports:$Global:coverageOutputXPlat" "-targetdir:$coverageReportDirectory" "-reporttypes:Html;JsonSummary"
	& type "$coverageReportDirectory\Summary.json"
}

function GenerateCoverageReport
{
	"Generate coverage report"	
	& $reportGeneratorExe "-reports:$coverageOutputXPlat;$coverageOutputFullFw" "-targetdir:$coverageReportDirectory" "-reporttypes:Html;JsonSummary"
	& type "$coverageReportDirectory\Summary.json"
}

Main